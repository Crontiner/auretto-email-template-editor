��    B      ,      <      <     =     N     a     v     �     �  	   �     �     �     �     �     �     �                  
   /     :  
   N     Y     _  )   k  "   �     �     �     �     �     
          0  	   B  
   L     W     h          �     �     �     �     �     �     �     �     �                    /     @     P     Y     g     o     �     �     �     �  :   �  )   �                8  
   Q     \  	   j  �  t     8     P     k     �     �      �     �     �     �     �                     1     A     X     f     r     �     �     �  &   �  /   �  -        9  
   O  "   Z     }  #   �     �     �     �     �     �     	          -     5     F     O     g          �     �     �     �     �     �               %     :     A     U     \     h     t  8   �  6   �     �               5     G     \   Add New Template Add media elements Add new input fields All Templates Attachments Auretto E-mail Template Editor Cc Emails Datas to be import Datas to be sent Date Edit Template Email Email Format Email Templates Email for cc Email template Error info Files / Attachments From Name: From: Import data Import datas from previous plugin version Last 100 emails sent in this month Last 50 error in this month Name for cc New Template No Templates found in Trash. No Templates found. No data to import Parent Templates: Password: Plain text Receiver Address Receiver email address SMTP Enabled SMTP Settings Save Search Templates Send Send a test E-mail Send a test email Sender Email Sender Name Sender Setting Sender Settings Sender email Sender settings Sent E-mails Log Server Address: Settings Settings page Subject Successfully Sent Template Template Name Template URL: Template settings The SMTP is enabled but the data is not completely filled! This month there was no email sending yet To Type your password here Use WooCommerce template User name: View Template Your Name Project-Id-Version: Auretto Email Template Editor
Report-Msgid-Bugs-To: 
POT-Creation-Date: Sun Jul 22 2018 12:03:51 GMT+0200 (közép-európai nyári idő)
PO-Revision-Date: Thu Aug 02 2018 09:59:15 GMT+0200 (közép-európai nyári idő)
Last-Translator: tibi <paulovicstibor@gmail.com>
Language-Team: 
Language: Hungarian
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Generator: Loco - https://localise.biz/
X-Loco-Target-Locale: hu_HU Új sablon hozzáadása Csatolmányok hozzáadása Új input mező hozzáadása Összes sablon Csatolmányok Auretto e-mail sablonszerkesztő Másolatot kapnak Áthozható adatok Küldendő adatok Dátum Sablon szerkesztése E-mail E-mail formátum E-mail sablonok Email (másolatot kap) E-mail sablon Hibaüzenet Fájlok / Csatolmányok Feladó neve Feladó Adatok importálása Előző plugin adatainak importálása Utolsó 100 kiküldött email ebben a hónapban Utolsó 50 kiküldési hiba ebben a hónapban Név (másolatot kap) Új sablon Nem található sablon a kukában. Nem található sablon. Nem található feldolgozható adat Szülő sablonok Jelszó Egyszerű szöveges Fogadó címe Fogadó e-mail címe SMTP aktiválva SMTP beállítások Mentés Sablon keresése Küldés Teszt üzenet küldése Teszt üzenet küldése Küldő e-mail Küldő neve Küldő beállításai Feladó beállításai Küldő email címe Küldő beállításai Kiküldött emailek Szerver címe Beállítások Beállítások oldal Tárgy Sikeresen elküldve Sablon Sablon neve Sablon URL: Sablon beállítások Az SMTP aktiválva, de még nincs kitöltve minden adat. Ebben a hónapban még nem történt e-mail kiküldés Fogadó Írd ide a jelszavad WooCommerce sablon használat Felhasználónév Sablon megtekintése Neved 