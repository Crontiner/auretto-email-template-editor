jQuery(function($) {

	if ( $('#auretto_email_editor_settings_metabox, body.auretto_email_editor_page_default-email-template-editor-page').length ) {
		if ( $('#auretto_email_editor_settings_metabox .inside').length ) {
			var $inside = $('#auretto_email_editor_settings_metabox .inside');
		} else {
			var $inside = $('body.auretto_email_editor_page_default-email-template-editor-page .inside');
		}

		var $subject = $inside.find('input[name="subject"]');
		var $sender_name = $inside.find('input[name="sender_name"]');
		var $sender_email = $inside.find('input[name="sender_email"]');

		$( '<div id="aee_inputs"></div>' ).insertAfter( "#titlediv" );
		$subject.detach().appendTo('#aee_inputs');

		$('#titlediv label#title-prompt-text').html( $('input[name="post_title_placeholder"]').val() );


		// Content changer
		$('div.menu-section ul li').click(function(){
			var $this = $(this);
			var selectedItem = $this.attr('data-menu-item');
			var $contentWrapper = $this.closest('.inside').find('.content-wrapper');

			$this.closest('ul').find('li.active').removeClass('active');
			$this.addClass('active');

			$contentWrapper.find('.content-section').each(function() {
				$(this).removeClass('active');
				if ( $(this).hasClass(selectedItem) === true ) { $(this).addClass('active'); }
			});
		});
	}

	/*
	 * Select/Upload image(s) event
	 */
	$('body').on('click', '#auretto_email_editor_settings_metabox div.upload-box', function(e){
			e.preventDefault();
			var button = $(this),
					custom_uploader = wp.media({
			title: 'Insert image',
			library : {
					// uncomment the next line if you want to attach image to the current post
					// uploadedTo : wp.media.view.settings.post.id,
					//type : 'image'
			},
			button: {
					text: 'Use this image' // button label text
			},
			multiple: true // for multiple image selection set to true

		}).on('select', function() { // it also has "open" and "close" events

					/*
					var attachment = custom_uploader.state().get('selection').first().toJSON();
					$(button).removeClass('button').html('<img class="true_pre_image" src="' + attachment.url + '" style="max-width:95%;display:block;" />').next().val(attachment.id).next().show();
					*/

					// if you sen multiple to true, here is some code for getting the image IDs

					var attachments = custom_uploader.state().get('selection'),
							attachment_ids = new Array();

					var $mediaFiles = $('#auretto_email_editor_settings_metabox .content-section.files_attachments ul.media_files');
					var sampleHTML = $mediaFiles.find('li.sample')[0].outerHTML;

					attachments.each(function(attachment) {
							var $attrs = attachment['attributes'];
							var fileName = $attrs['filename'];
							var editLink = $attrs['editLink'];
							var attachmentID = attachment['id'];

							$mediaFiles.append(sampleHTML);
							if ( $mediaFiles.find('li.sample').length > 1 ) {
								$mediaFiles.find('li.sample').removeClass('sample');
								$mediaFiles.find('li').eq(0).addClass('sample');
							}

							$mediaFiles.find('li').last().find('a').html(fileName);
							$mediaFiles.find('li').last().find('a').attr('href', editLink);
							$mediaFiles.find('li').last().find('input').val(attachmentID);
					});
			})
			.open();
	});


	// Delete attachment from post
	$('body').on('click', '#auretto_email_editor_settings_metabox ul.media_files li .remove_btn', function(e) {
		$(this).closest('li').remove();
	});


	// CC Input Fields - Add new input fields
	$('div.cc-input-fields button.cc-add-more-btn').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		var $fields = $this.closest('.cc-input-fields');
		var pattern = $fields.find('.cc-input-field:first-of-type')[0].outerHTML;

		$(pattern).insertBefore($this);
		$fields.find('.cc-input-field:last-of-type input').val("");
	});


	// CC Input Fields - Remove input fields
	$('body').on( 'click', 'div.cc-input-fields .cc-input-field div.remove_btn', function(e) {
		e.preventDefault();
		var $this = $(this);

		$this.closest('.cc-input-field').fadeOut(function() {
			$(this).remove();
		});
	});


	// Fix the admin notice panel
	if ( $('div.notice.incomplete-smtp-data').length > 1 ) {
		$('div.notice.incomplete-smtp-data').each(function(key, val){
			if ( key > 0 ) { $(this).remove(); }
		});
	}


});
