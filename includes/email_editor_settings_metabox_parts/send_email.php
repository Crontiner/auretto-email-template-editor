<input type="text" value="<?php echo $this->aee->functions->get_default_sender_email(); ?>" name="aee_send_email_address" placeholder="<?php _e('Receiver email address', Auretto_Email_Editor::TEXTDOMAIN); ?>" />
<input type="submit" value="<?php _e('Send', Auretto_Email_Editor::TEXTDOMAIN); ?>" name="aee_send_email_manual" class="button button-primary button-large" />

<br><br><hr><br>
<h4><?php _e('Datas to be sent', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>
<?php
	//echo "<pre>"; var_dump($template_datas); echo "</pre>";

	$tr = "";
	if ( !empty($template_datas) ) {
		foreach ($template_datas as $key => $val) {

			if ( $key == 'attachments' && !empty($val) ) {
				$val = unserialize($val);

				if ( is_array($val) ) {
					$media_urls = array();
					foreach ($val as $key2 => $value2) {
						$media_urls []= '<a target="_blank" href="'. $value2['url'] .'">'. basename($value2['url']) .'</a>';
					}
					$val = implode(', ', $media_urls);
				}
			}
			else if ( $key == 'attachments' && empty($val) ) { $val = ""; }

			if ( $key == 'smtp_pass' && !empty($val) ) {
				$val = str_repeat('&#10003;', strlen($val));
			}
			if ( $key == 'msg' && !empty($val) ) {
				$val = strip_tags($this->aee->functions->short_text($val, 200));
			}

			if ( ($key == 'main_cc_emails') || ($key == 'postmeta_cc_emails') || ($key == 'cc_emails') && !empty($val)) {
				$val_temp = "";
				foreach ($val as $cc_email => $cc_name) { $val_temp .= '<li>'. $cc_email .' - '. $cc_name .'</li>'; }
				$val = '<ul>'. $val_temp .'</ul>';
			}
			//if ($key == 'cc_emails') { $val = ""; }

			if ( $key == 'smtp_enabled' ) { $val = strtoupper(json_encode($val)); }
			if ( $key == 'use_email_template' ) { $val = strtoupper(json_encode($val)); }
			if ( $key == 'use_wc_template' ) { $val = strtoupper(json_encode($val)); }

			if ( !empty($val) ) {
				$tr .= 	'<tr>'.
									'<th>'. $key .'</th>'.
									'<td>'. $val .'</td>'.
								'</tr>';
			}
		}
		echo '<table class="template_datas">'. $tr .'</table>';
	}
?>
