<?php
$sent_log = get_option('aee_sent_emails_log_'. date('Y-m'));
$sent_error_log = get_option('aee_sent_emails_error_log_'. date('Y-m'));


if ( !empty($sent_log) ) {
	$sent_log = array_reverse($sent_log);

	foreach ($sent_log as $key => $val) {
		if ( $val['post_id'] != intval($_GET['post']) ) { unset($sent_log[$key]); }
	}
	$sent_log = array_slice($sent_log, 0, 100);
}

if ( !empty($sent_error_log) ) {
	$sent_error_log = array_reverse($sent_error_log);

	foreach ($sent_error_log as $key => $val) {
		if ( $val['post_id'] != intval($_GET['post']) ) { unset($sent_error_log[$key]); }
	}
	$sent_error_log = array_slice($sent_error_log, 0, 50);
}

if ( !empty($sent_log) ) {
	$tr = "";
	foreach ($sent_log as $key => $val) {

		$tr .= 	'<tr data-ready="'. json_encode($val['email_sent']) .'">'.
							'<td>'. json_encode($val['email_sent']) .'</td>'.
							'<td>'. $val['receiver_email'] .'</td>'.
							'<td>'. date('Y-m-d H:i', $val['date']) .'</td>'.
						'</tr>';
	}

	echo 	'<h5>'. __('Last 100 emails sent in this month', Auretto_Email_Editor::TEXTDOMAIN) .'</h5>';
	echo 	'<table class="sent_emails_log">'.
					'<tr>
						<th>'. __('Successfully Sent', Auretto_Email_Editor::TEXTDOMAIN) .'</th>
						<th>'. __('Receiver Address', Auretto_Email_Editor::TEXTDOMAIN) .'</th>
						<th>'. __('Date', Auretto_Email_Editor::TEXTDOMAIN) .'</th>
					</tr>'.
					$tr .
				'</table>';
} else {
	echo 	'<h5>'. __('This month there was no email sending yet', Auretto_Email_Editor::TEXTDOMAIN) .'</h5>';
}

if ( !empty($sent_error_log) ) {
	$tr = "";
	foreach ($sent_error_log as $key => $val) {

		$tr .= 	'<tr>'.
							'<td>'. implode(', ', $val['to']) .'</td>'.
							'<td>'. $val['HTTP_REFERER'] .'</td>'.
							'<td>'. $val['error_info'] .'</td>'.
							'<td>'. date('Y-m-d H:i', $val['date']) .'</td>'.
						'</tr>';
	}

	echo 	'<br><h5>'. __('Last 50 error in this month', Auretto_Email_Editor::TEXTDOMAIN) .'</h5>';
	echo 	'<table class="sent_emails_log sent_emails_error_log">'.
					'<tr>
						<th>'. __('To', Auretto_Email_Editor::TEXTDOMAIN) .'</th>
						<th>'. __('HTTP referer', Auretto_Email_Editor::TEXTDOMAIN) .'</th>
						<th>'. __('Error info', Auretto_Email_Editor::TEXTDOMAIN) .'</th>
						<th>'. __('Date', Auretto_Email_Editor::TEXTDOMAIN) .'</th>
					</tr>'.
					$tr .
				'</table>';
}
?>
