<div class="upload-box button button-primary button-large">
	<div class="dashicons dashicons-upload"></div>
	<span><?php _e('Add media elements', Auretto_Email_Editor::TEXTDOMAIN); ?></span>
</div>
<div class="clearfix"></div>

<div class="all_media_files_sizes"><?php echo $file_sizes; ?></div>
<ul class="media_files">
	<li class="sample">
		<a href="" target="_blank"></a>
		<div class="remove_btn dashicons dashicons-trash"></div>
		<input type="hidden" value="" name="aee_attachments[]" />
	</li>
	<?php echo $media_files; ?>
</ul>
