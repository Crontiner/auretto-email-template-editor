<?php

/***********************/
/*     ADD FILTERS     */
/***********************/

class AurettoEmailFilters {

	public function __construct($aee) {
		$this->aee = $aee;
	}

	public function templateRedirects() {
		global $post;

		if ( is_singular(Auretto_Email_Editor::MAIN_CPT_NAME) ) {
			wp_redirect( home_url() ); exit;
		}
	}

}
