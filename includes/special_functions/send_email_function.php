<?php

class AurettoEmailSendMailFunction {

	public function __construct($aee) {
		$this->aee = $aee;
	}

	/*
	$extra_datas = array(
		'receiver_email' => $_POST['aee_send_email_address'],
		//'reply_to_email' => 'testmail_bhj44n32@gmail.com',
		//'reply_to_name' => 'Lorem Ipsum',
		'replaceable_content_words' => array(
																					'%name%' => 'Teszt elek',
																					'%password%' => wp_generate_password(8, false, false),
																					'%link%' => get_site_url(),
																					'%email_title%' => get_post_meta($post_id, 'aee_subject', true),
																					'%header_text%' => get_post_meta($post_id, 'aee_subject', true),
																				),
		'added_attachments' => array( 44392,
																	'http://munkaorak.auretto.works/wp-content/uploads/2018/08/photo-by-todd-desantis-w_9moguwr08.jpg',
																	'http://munkaorak.auretto.works/wp-content/uploads/2016/08/v56dsc56ds.pdf',
																),
	);

	// Replace Words in template
	$extra_datas['replaceable_content_words']['%email_body_content%'] = $this->aee->functions->replace_words($this->aee->functions->get_the_content_by_id($post_id, TRUE), $extra_datas);
	*/

	function send_email($template_post_id = "", $extra_datas = "") {
		if ( intval($template_post_id) > 0 ) {  }
		else { return ""; }

		$email_sent = FALSE;
		$headers = array();
		$sent_log = get_option('aee_sent_emails_log_'. date('Y-m'));
		if ( empty($sent_log) ) { $sent_log = array(); }

		$template_data = $this->aee->functions->get_email_template($template_post_id);


		// Attachments

		$attachments_array = "";
		if ( isset($template_data['attachments']) && !empty($template_data['attachments']) ) {
			$attachments_array = unserialize($template_data['attachments']);

			if ( isset($attachments_array[0]['path']) && !empty($attachments_array[0]['path']) ) {
				$attachments_array_temp = array();
				foreach ($attachments_array as $key => $val) { $attachments_array_temp []= $val['path']; }
				$attachments_array = $attachments_array_temp;
			} else {
				$attachments_array = "";
			}
		}


		// Extra datas

		$receiver_email = "";
		$reply_to_email = "";
		$reply_to_name = "";
		$added_attachments = "";

		if ( !empty($extra_datas) && is_array($extra_datas) ) {
			if ( isset($extra_datas['receiver_email']) ) { $receiver_email = $extra_datas['receiver_email']; }
			else { $receiver_email = $template_data['sender_email']; }

			if ( isset($extra_datas['reply_to_email']) ) 	{ $reply_to_email = $extra_datas['reply_to_email']; }
			if ( isset($extra_datas['reply_to_name']) ) 	{ $reply_to_name 	= $extra_datas['reply_to_name']; }
		}


		// Extra datas: Added Attachments
		if ( isset($extra_datas['added_attachments']) ) {
			if ( !empty($extra_datas['added_attachments']) && is_array($extra_datas['added_attachments']) ) {
				foreach ($extra_datas['added_attachments'] as $key => $media_file) {
					$media_id = 0;

					if ( is_numeric($media_file) ) {
						if ( get_post_type($media_file) == 'attachment' ) { $media_id = intval($media_file); }
					} else {
						$media_file = $this->aee->functions->get_attachment_id_by_url($media_file);
						if ( intval($media_file) > 0 ) {
							$media_id = intval($media_file);
						}
					}

					if ( $media_id > 0 ) {
						$media_file_path = get_attached_file($media_id);

						if ( !empty($media_file_path) ) {
							if ( empty($attachments_array) && !is_array($attachments_array) ) { $attachments_array = array(); }
							$attachments_array []= $media_file_path;
						}
					}

				}
			}
		}


		// Set sender data
		if 	( isset($template_data['sender_email']) && !empty($template_data['sender_email']) &&
					isset($template_data['sender_name']) && !empty($template_data['sender_name'])
				) {
			$headers[] = 'From: '. $template_data['sender_name'] .' < '. $template_data['sender_email'] .' >';
		}
		else if ( isset($template_data['sender_email']) && !empty($template_data['sender_email']) && empty($template_data['sender_name']) ) {
			$headers[] = 'From: '. $template_data['sender_email'];
		}


		// Set Cc emails
		if ( isset($template_data['cc_emails']) && !empty($template_data['cc_emails']) ) {
			foreach ($template_data['cc_emails'] as $cc_email => $cc_name) {

				if ( !empty($cc_name) && !empty($cc_email) ) {
					$headers []= "Cc: {$cc_name} < {$cc_email} >";
				}
				else if ( empty($cc_name) && !empty($cc_email) ) {
					$headers []= 'Cc: '. $cc_email;
				}
			}
		}


		// Use SMTP sending
		add_action( 'phpmailer_init', array($this->aee->functions,'smtp_mailer_config'), 10, 1);


		// E-mail sending
		$to = $receiver_email;
		$subject = $template_data['subject'];

		if ( isset($template_data['use_wc_template']) && ($template_data['use_wc_template'] === TRUE) && ($this->aee->functions->is_woocommerce_active() === TRUE) ) {
			$body = $this->aee->functions->get_the_content_by_id($template_post_id, TRUE);
			$body = $this->aee->functions->wc_email_template($subject, $body);
		} else if ( isset($template_data['use_email_template']) && ($template_data['use_email_template'] === TRUE) ) {
			$body = $this->aee->functions->get_template_content(intval($_POST['aee_actual_post_id']));
		} else {
			$body = $this->aee->functions->get_the_content_by_id($template_post_id, TRUE);
		}
		$body = $this->aee->functions->replace_words($body, $extra_datas);


		if ( isset($template_data['send_mode']) && ($template_data['send_mode'] == 'html') ) {
			$headers[] = 'Content-Type: text/html; charset=UTF-8';
		} else {
			$headers[] = 'Content-Type: text/plain; charset=UTF-8';
		}


		if ( wp_mail( $to, $subject, $body, $headers, $attachments_array ) === FALSE ) { $email_sent = FALSE; }
		else { $email_sent = TRUE; }


		// Logging Results

		$sent_log []= array(
										'email_sent' => $email_sent,
										'receiver_email' => $receiver_email,
										'post_id' => $template_post_id,
										'date' => strtotime('NOW'),
									);
		update_option('aee_sent_emails_log_'. date('Y-m'), $sent_log, false);

		return $email_sent;
	}
}
