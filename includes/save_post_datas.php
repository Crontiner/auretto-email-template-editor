<?php

/********************************/
/*     SAVE POST META DATAS     */
/********************************/

class AurettoEmailSavePostDatas {

	public function __construct($aee) {
		$this->aee = $aee;
	}

	function aurettoEmailEditorSavePostdatas($post_id) {
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return ""; }
		global $post;

		if ($_POST['post_type'] == Auretto_Email_Editor::MAIN_CPT_NAME) {

			if ( 	!isset( $_POST[Auretto_Email_Editor::MAIN_METABOX_NAME .'-field-'. intval($post->ID)] ) ||
						!wp_verify_nonce( $_POST[Auretto_Email_Editor::MAIN_METABOX_NAME .'-field-'. intval($post->ID)], Auretto_Email_Editor::MAIN_METABOX_NAME .'-action-'. intval($post->ID) ) ) {

				//print 'Sorry, your nonce did not verify.'; exit;

			} else {

				if ( !isset($_POST['subject']) || empty($_POST['subject']) ) { $_POST['subject'] = ""; }
				if ( !isset($_POST['sender_name']) || empty($_POST['sender_name']) ) { $_POST['sender_name'] = ""; }
				if ( !isset($_POST['email_send_mode']) || empty($_POST['email_send_mode']) ) { $_POST['email_send_mode'] = ""; }
				if ( !isset($_POST['sender_email']) || empty($_POST['sender_email']) ) { $_POST['sender_email'] = ""; }

				update_post_meta($post->ID, "aee_subject", $_POST['subject']);
				update_post_meta($post->ID, "aee_sender_name", $_POST['sender_name']);
				update_post_meta($post->ID, "aee_email_send_mode", $_POST['email_send_mode']);
				update_post_meta($post->ID, "aee_sender_email", $_POST['sender_email']);


				// Save attachments
				if ( isset($_POST['aee_attachments']) ) {

					$attachments_array = array();
					if ( is_array($_POST['aee_attachments']) && !empty($_POST['aee_attachments']) ) {
						foreach ($_POST['aee_attachments'] as $key => $attachment_id) {
							if ( intval($attachment_id) > 0 ) {
								$attachments_array []= intval($attachment_id);
							}
						}
						$attachments_array = array_values(array_filter(array_unique($attachments_array)));
					}

					update_post_meta($post->ID, "aee_attachments", $attachments_array);
				}


				// Save Sender settings
				if ( isset($_POST['sender_name']) ) { update_post_meta($post->ID, "aee_sender_name", $_POST['sender_name']); }
				if ( isset($_POST['sender_email']) ) { update_post_meta($post->ID, "aee_sender_email", $_POST['sender_email']); }


				// Save CC input fields
				if ( isset($_POST['cc_emails']) && is_array($_POST['cc_emails']) ) {
					$cc_datas = array();

					foreach ($_POST['cc_emails'] as $key => $cc_email) {
						if ( is_email($cc_email) ) {
							$cc_name = "";
							if ( isset($_POST['cc_names'][$key]) && !empty($_POST['cc_names'][$key]) ) { $cc_name = $_POST['cc_names'][$key]; }

							$cc_datas[$cc_email] = $cc_name;
						}
					}

					update_post_meta($post->ID, "aee_cc_emails", $cc_datas);
				}


				// Save Template Override
				if ( isset($_POST['email_template_url']) ) {

					if ( !empty($_POST['email_template_url']) ) {
						if ( $this->aee->functions->email_template_url_validation($_POST['email_template_url']) !== TRUE ) { $_POST['email_template_url'] = ""; }
					}

					update_post_meta($post->ID, "aee_email_template_url", $_POST['email_template_url']);
				}


				// Use WooCommerce Template
				if ( isset($_POST['use_wc_template']) && (intval($_POST['use_wc_template']) == 1) ) {
					update_post_meta($post->ID, "aee_use_wc_template", TRUE );
				} else {
					update_post_meta($post->ID, "aee_use_wc_template", FALSE );
				}


				// Send a test email
				if ( isset($_POST['aee_send_email_manual']) && isset($_POST['aee_actual_post_id']) && is_email($_POST['aee_send_email_address']) ) {

					$extra_datas = array(
							'receiver_email' => $_POST['aee_send_email_address'],
							//'reply_to_email' => 'testmail_bhj44n32@gmail.com',
							//'reply_to_name' => 'Lorem Ipsum',
							'replaceable_content_words' => array(
																										'%name%' => 'Teszt elek',
																										'%password%' => wp_generate_password(8, false, false),
																										'%link%' => get_site_url(),
																										'%email_title%' => get_post_meta($post_id, 'aee_subject', true),
																										'%header_text%' => get_post_meta($post_id, 'aee_subject', true),
																									),
							'added_attachments' => array( 44392,
																						'http://munkaorak.auretto.works/wp-content/uploads/2018/08/photo-by-todd-desantis-w_9moguwr08.jpg',
																						'http://munkaorak.auretto.works/wp-content/uploads/2016/08/v56dsc56ds.pdf',
																					),
					);

					// Replace Words in template
					$extra_datas['replaceable_content_words']['%email_body_content%'] = $this->aee->functions->replace_words($this->aee->functions->get_the_content_by_id($post_id, TRUE), $extra_datas);


					$this->aee->sendMailFunction->send_email( intval($_POST['aee_actual_post_id']), $extra_datas );
				}
			}
		}
	}

}
