<?php

/***********************/
/*     ADD ACTIONS     */
/***********************/

class AurettoEmailActions {

	public function __construct($aee) {
		$this->aee = $aee;
	}


	/******************************/
	/*     DEFINE EDITOR PAGE     */
	/******************************/

	public function defaultEmailTemplateEditorPageMenuItem() {
		add_submenu_page(
				'edit.php?post_type='. Auretto_Email_Editor::MAIN_CPT_NAME,
				__( 'Settings', Auretto_Email_Editor::TEXTDOMAIN ),
				__( 'Settings', Auretto_Email_Editor::TEXTDOMAIN ),
				'manage_options',
				Auretto_Email_Editor::MAIN_SETTINGS_PAGE_NAME,
				array($this, 'defaultEmailTemplateEditorPageCallback')
		);
	}

	public function defaultEmailTemplateEditorPageCallback() {

		// Save template parts
		if (isset($_POST['save_default_email_template_parts'])) {

			if ( 	!isset( $_POST[Auretto_Email_Editor::MAIN_SETTINGS_PAGE_NAME .'-field'] ) ||
						!wp_verify_nonce( $_POST[Auretto_Email_Editor::MAIN_SETTINGS_PAGE_NAME .'-field'], Auretto_Email_Editor::MAIN_SETTINGS_PAGE_NAME .'-action') ) {

				//print 'Sorry, your nonce did not verify.'; exit;

			} else {

				$settings_array = array( 	'email_template_url' => "",
																	'email_format' => "",

																	'smtp_enabled' => "",
																	'smtp_host' => "",
																	'smtp_port' => "",
																	'smtp_user' => "",
																	'smtp_pass' => "",
																	'smtp_secure' => "",
																	'smtp_from' => "",
																	'smtp_name' => "",

																	'sender_name' => "",
																	'sender_email' => "",
															);


				foreach ($settings_array as $inp_name => $val) {
					if ( isset($_POST[$inp_name]) ) {
						$settings_array[$inp_name] = $_POST[$inp_name];
					}
				}

				if ( isset($settings_array['email_template_url']) && !empty($settings_array['email_template_url']) ) {
					if ( $this->aee->functions->email_template_url_validation($settings_array['email_template_url']) !== TRUE ) {
						$settings_array['email_template_url'] = "";
					}
				}


				// Add CC input fields
				if ( isset($_POST['cc_emails']) && is_array($_POST['cc_emails']) ) {
					$cc_datas = array();

					foreach ($_POST['cc_emails'] as $key => $cc_email) {
						if ( is_email($cc_email) ) {
							$cc_name = "";
							if ( isset($_POST['cc_names'][$key]) && !empty($_POST['cc_names'][$key]) ) { $cc_name = $_POST['cc_names'][$key]; }

							$cc_datas[$cc_email] = $cc_name;
						}
					}

					$settings_array['cc_emails'] = $cc_datas;
				}

				$settings_array['last_updated'] = strtotime('NOW');
				update_option( 'aee_email_template_settings', $settings_array, false );
			}
		}


		// Import datas from previous plugin version
		if (isset($_POST['import_datas_from_previous_plugin_version_submit'])) {
			$prev_plugin_datas = $this->aee->importPrevPluginFunctions->get_the_prev_plugin_templates_data();

			if ( $prev_plugin_datas !== FALSE ) {
				foreach ($prev_plugin_datas as $template_id => $template_data) {

					$post_data = array(
						'post_title'		=> wp_strip_all_tags( $template_data['email_template_name'] ),
						'post_name'			=> $template_data['email_template_slug'],
						'post_content'	=> $template_data['msg'],
						'post_status'		=> 'draft',
						'post_type'			=> Auretto_Email_Editor::MAIN_CPT_NAME,
						'meta_input'		=> array(
																			'aee_subject' => $template_data['subject'],
																			'aee_sender_email' => $template_data['sender'],
																			'aee_sender_name' => $template_data['sender_name'],
																			'aee_attachments' => $template_data['attachments'],
																		),
					);
					wp_insert_post( $post_data );
				}
			}

			// Turn off the old plugin
			if ( is_plugin_active('email_sablon_szerkeszto/index.php') ) {
				deactivate_plugins('email_sablon_szerkeszto/index.php');
			}
		}

		// /Save


		$settings_array = get_option('aee_email_template_settings');
		if ( empty($settings_array) ) { $settings_array = array(); }

		if ( !isset($settings_array['email_format']) ) { $settings_array['email_format'] = "plain"; }
		if ( !isset($settings_array['sender_name']) || empty($settings_array['sender_name']) ) {
			$settings_array['sender_name'] = $this->aee->functions->get_default_sender_name();
		}
		if ( !isset($settings_array['sender_email']) || empty($settings_array['sender_email']) ) {
			$settings_array['sender_email'] = $this->aee->functions->get_default_sender_email();
		}

		$cc_emails_array = $this->aee->functions->get_cc_emails_from_main_settings();

		$is_woocommerce_active_class = "";
		if ( $this->aee->functions->is_woocommerce_active() ) {
			$is_woocommerce_active_class = 'is_woocommerce_active';
		}

		?>
		<div class="wrap inside <?php echo $is_woocommerce_active_class; ?> ">
			<h2><?php _e('Settings page', Auretto_Email_Editor::TEXTDOMAIN) ?></h2>
			<br><hr><br>

			<form method="post" action="" autocomplete="off">
				<div class="menu-section">
					<ul class="menu">
						<li data-menu-item="template_settings" class="active">
							<i class="material-icons">description</i>
							<span><?php _e('Template settings', Auretto_Email_Editor::TEXTDOMAIN); ?></span>
						</li>
						<li data-menu-item="smtp_settings">
							<i class="material-icons">security</i>
							<span><?php _e('SMTP Settings', Auretto_Email_Editor::TEXTDOMAIN); ?></span>
						</li>
						<li data-menu-item="sender_settings">
							<i class="material-icons">settings</i>
							<span><?php _e('Sender Settings', Auretto_Email_Editor::TEXTDOMAIN); ?></span>
						</li>
						<?php
						if ( $this->aee->importPrevPluginFunctions->check_prev_plugin_is_active() === TRUE ) {
							?>
							<li data-menu-item="import_datas_from_previous_plugin_version">
								<i class="material-icons">import_export</i>
								<span><?php _e('Import data', Auretto_Email_Editor::TEXTDOMAIN); ?></span>
							</li>
							<?php
						}
						?>
					</ul>
					<input type="submit" value="<?php _e('Save', Auretto_Email_Editor::TEXTDOMAIN); ?>" name="save_default_email_template_parts" class="button button-primary button-large" />
				</div>
				<div class="content-wrapper">

					<div class="content-section template_settings active">
						<h4><?php _e('Email template', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>

						<label for="email_template_url"><?php _e('Template URL:', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
						<input type="text" value="<?php echo $settings_array['email_template_url']; ?>" name="email_template_url" />

						<h4><?php _e('Email Format', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>
						<div class="radio-form">
							<div>
								<input type="radio" id="plain" name="email_format" value="plain" <?php checked( $settings_array['email_format'], 'plain' ); ?> />
								<label for="plain"><?php _e('Plain text', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
							</div>
							<div>
								<input type="radio" id="html" name="email_format" value="html" <?php checked( $settings_array['email_format'], 'html' ); ?> />
								<label for="html"><?php _e('HTML', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="content-section smtp_settings">
						<h4><?php _e('SMTP Settings', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>

						<div class="checkbox-form">
							<input type="checkbox" id="smtp_enabled" name="smtp_enabled" value="1" <?php checked( $settings_array['smtp_enabled'], '1' ); ?> />
							<label for="smtp_enabled"><?php _e('SMTP Enabled', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
						</div>

						<div class="<?php if ( $this->aee->functions->smtp_is_enabled() === TRUE ) { echo 'smtp-enabled'; } else { echo 'smtp-disabled'; } ?>">
							<label id="smtp_host"><?php _e('Server Address:', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
							<input type="text" value="<?php echo $settings_array['smtp_host']; ?>" name="smtp_host" id="smtp_host" placeholder="e.g smtp.gmail.com" />

							<label id="smtp_port"><?php _e('Port:', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
							<input type="number" value="<?php echo $settings_array['smtp_port']; ?>" name="smtp_port" id="smtp_port" placeholder="e.g 465" />

							<label id="smtp_user"><?php _e('User name:', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
							<input type="text" value="<?php echo $settings_array['smtp_user']; ?>" name="smtp_user" id="smtp_user" placeholder="e.g johndoe@example.com" />

							<label id="smtp_pass"><?php _e('Password:', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
							<input type="password" value="<?php echo $settings_array['smtp_pass']; ?>" name="smtp_pass" id="smtp_pass" placeholder="<?php _e('Type your password here', Auretto_Email_Editor::TEXTDOMAIN); ?>" />

							<label id="smtp_from"><?php _e('From:', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
							<input type="text" value="<?php echo $settings_array['smtp_from']; ?>" name="smtp_from" id="smtp_from" placeholder="you@yourdomail.com" />

							<label id="smtp_name"><?php _e('From Name:', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
							<input type="text" value="<?php echo $settings_array['smtp_name']; ?>" name="smtp_name" id="smtp_name" placeholder="<?php _e('Your Name', Auretto_Email_Editor::TEXTDOMAIN); ?>" />

							<div class="radio-form">
								<label><?php _e('Secure:', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
								<div>
									<input type="radio" id="smtp_secure_ssl" name="smtp_secure" value="SSL" <?php checked( $settings_array['smtp_secure'], 'SSL' ); ?> />
									<label for="smtp_secure_ssl"><?php _e('SSL', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
								</div>
								<div>
									<input type="radio" id="smtp_secure_tls" name="smtp_secure" value="TLS" <?php checked( $settings_array['smtp_secure'], 'TLS' ); ?> />
									<label for="smtp_secure_tls"><?php _e('TLS', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
								</div>
							</div>
						</div>
					</div>

					<div class="content-section sender_settings">
						<h4><?php _e('Sender Settings', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>
						<input type="text" value="<?php echo $settings_array['sender_name']; ?>" name="sender_name" placeholder="<?php _e('Sender Name', Auretto_Email_Editor::TEXTDOMAIN); ?>" />
						<input type="email" value="<?php echo $settings_array['sender_email']; ?>" name="sender_email" placeholder="<?php _e('Sender email', Auretto_Email_Editor::TEXTDOMAIN); ?>" />

						<div class="cc-input-fields">
							<h4><?php _e('Cc Emails', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>
							<?php
							if ( $cc_emails_array !== FALSE ) {
								foreach ($cc_emails_array as $cc_email => $cc_name) {
									?>
									<div class="cc-input-field">
										<input type="text" value="<?php echo $cc_name; ?>" name="cc_names[]" placeholder="<?php _e('Name for cc', Auretto_Email_Editor::TEXTDOMAIN); ?>" />
										<input type="email" value="<?php echo $cc_email; ?>" name="cc_emails[]" placeholder="<?php _e('Email for cc', Auretto_Email_Editor::TEXTDOMAIN); ?>" />
										<div class="remove_btn dashicons dashicons-trash"></div>
									</div>
									<?php
								}
							} else {
								?>
								<div class="cc-input-field">
									<input type="text" value="" name="cc_names[]" placeholder="<?php _e('Name for cc', Auretto_Email_Editor::TEXTDOMAIN); ?>" />
									<input type="email" value="" name="cc_emails[]" placeholder="<?php _e('Email for cc', Auretto_Email_Editor::TEXTDOMAIN); ?>" />
									<div class="remove_btn dashicons dashicons-trash"></div>
								</div>
								<?php
							}
							?>

							<button class="cc-add-more-btn button button-primary button-large">
								<div class="dashicons dashicons-plus"></div>
								<span><?php _e('Add new input fields', Auretto_Email_Editor::TEXTDOMAIN); ?></span>
							</button>
						</div>
					</div>

					<?php
					if ( $this->aee->importPrevPluginFunctions->check_prev_plugin_is_active() === TRUE ) {
						$prev_plugin_datas = $this->aee->importPrevPluginFunctions->get_the_prev_plugin_templates_data();

						if ( $prev_plugin_datas !== FALSE ) {
							$prev_plugin_datas_temp = "";

							foreach ($prev_plugin_datas as $template_id => $template_array) {
								foreach ($prev_plugin_datas[$template_id] as $key => $val) {

									if ( is_array($val) ) { $val = json_encode($val); }

									if ( !empty($val) ) {
										$prev_plugin_datas_temp .=
											'<tr>
												<th>'. $key .'</th>
												<td>'. $val .'</td>
											</tr>';
									}
								}
								$prev_plugin_datas_temp .= '<tr><td colspan="2"><br><hr><br></td></tr>';
							}

							$prev_plugin_datas_temp = '<table>'. $prev_plugin_datas_temp .'</table>';
						} else {
							$prev_plugin_datas_temp = '<p>'. __('No data to import', Auretto_Email_Editor::TEXTDOMAIN) .'</p>';
						}
						$prev_plugin_datas = $prev_plugin_datas_temp;

						?>
						<div class="content-section import_datas_from_previous_plugin_version">
							<h4><?php _e('Import datas from previous plugin version', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>
							<input type="submit" value="Import all data" name="import_datas_from_previous_plugin_version_submit" class="button button-primary button-large" />

							<br><br><br>
							<h4><?php _e('Datas to be import', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>
							<?php echo $prev_plugin_datas; ?>
						</div>
						<?php
					}
					?>

				</div>
				<?php wp_nonce_field(	Auretto_Email_Editor::MAIN_SETTINGS_PAGE_NAME .'-action',
															Auretto_Email_Editor::MAIN_SETTINGS_PAGE_NAME .'-field'); ?>
			</form>
		</div>
		<?php
	}

	/* --- /DEFINE EDITOR PAGE --- */



	/*****************************/
	/*     LOG MAILER ERRORS     */
	/*****************************/

	/*
	Make error for testing:
		\wp-includes\pluggable.php

		$from_email = apply_filters( 'wp_mail_from', $from_email );
		$from_email = "";
	*/

	public function logMailerErrors($error) {
		global $post;

		$post_id = "";
		$subject = "";
		$to = "";

		if ( isset($post->ID) && ($post->ID > 0) ) { $post_id = $post->ID; }
		if ( isset($error->error_data['wp_mail_failed']['subject']) ) { $subject = $error->error_data['wp_mail_failed']['subject']; }
		if ( isset($error->error_data['wp_mail_failed']['to']) ) 			{ $to = $error->error_data['wp_mail_failed']['to']; }


		$mail_error_log = get_option('aee_sent_emails_error_log_'. date('Y-m'));
		if ( empty($mail_error_log) ) { $mail_error_log = array(); }

		// Skip the same data
		if ( !empty($mail_error_log) ) {
			$last_log_item = end($mail_error_log);

			if ( isset($last_log_item['date']) ) {
				if ( $last_log_item['date'] == strtotime('NOW') ) { return false; }
			}
		}

		$mail_error_log []= array(
															'error_info' => $error->get_error_message(),
															'post_id' => $post_id,
															'subject' => $subject,
															'to' => $to,
															'HTTP_REFERER' => $_SERVER['HTTP_REFERER'],
															'date' => strtotime('NOW'),
														);
		update_option('aee_sent_emails_error_log_'. date('Y-m'), $mail_error_log, false);
	}

	/* --- /LOG MAILER ERRORS --- */


	/*************************/
	/*     ADMIN NOTICES     */
	/*************************/

	public function incompleteSmtpData() {
		$screen = get_current_screen();


		if ( 	($screen->post_type == Auretto_Email_Editor::MAIN_CPT_NAME) &&
					($screen->base == 'post' || $screen->base == 'edit') ) {

			if ( $this->aee->functions->smtp_is_enabled() === TRUE ) {
				if ( $this->aee->functions->smtp_is_filled() === FALSE ) {
					?>
					<div class="notice notice-error incomplete-smtp-data">
						<p><?php _e('The SMTP is enabled but the data is not completely filled!', Auretto_Email_Editor::TEXTDOMAIN); ?></p>
					</div>
					<?php
				}
			}

		}
	}

	/* --- /ADMIN NOTICES --- */

}
