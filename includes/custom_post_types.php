<?php

/***************/
/*     CPT     */
/***************/

class AurettoEmailCpt {

	public function registerPostType() {

		$labels = array(
			'name'               => __('Auretto E-mail Template Editor', Auretto_Email_Editor::TEXTDOMAIN),
			'singular_name'      => __('Email', Auretto_Email_Editor::TEXTDOMAIN),
			'menu_name'          => __('Email Templates', Auretto_Email_Editor::TEXTDOMAIN),
			'name_admin_bar'     => __('Email', Auretto_Email_Editor::TEXTDOMAIN),
			'add_new'            => __('Add New Template', Auretto_Email_Editor::TEXTDOMAIN),
			'add_new_item'       => __('Add New Template', Auretto_Email_Editor::TEXTDOMAIN),
			'new_item'           => __('New Template', Auretto_Email_Editor::TEXTDOMAIN),
			'edit_item'          => __('Edit Template', Auretto_Email_Editor::TEXTDOMAIN),
			'view_item'          => __('View Template', Auretto_Email_Editor::TEXTDOMAIN),
			'all_items'          => __('All Templates', Auretto_Email_Editor::TEXTDOMAIN),
			'search_items'       => __('Search Templates', Auretto_Email_Editor::TEXTDOMAIN),
			'parent_item_colon'  => __('Parent Templates:', Auretto_Email_Editor::TEXTDOMAIN),
			'not_found'          => __('No Templates found.', Auretto_Email_Editor::TEXTDOMAIN),
			'not_found_in_trash' => __('No Templates found in Trash.', Auretto_Email_Editor::TEXTDOMAIN),
		);

		$args = array(
			'labels'                => $labels,
			'description'           => "",
			'public'                => true,
			'show_in_nav_menus'     => true,
			'exclude_from_search'   => false,
			'has_archive'           => true,
			'menu_icon'             => 'dashicons-email-alt',
			'supports'              => array( 'title', 'editor', 'revisions' )
		);
		register_post_type('auretto_email_editor', $args);
	}
}
