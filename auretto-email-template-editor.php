<?php
/**
* Plugin Name: Auretto Email Template Editor
* Plugin URI: http://auretto.works
* Description:
* Version: 2.2
* Author: Auretto Works
* Author URI: http://auretto.works
* Text Domain: auretto-email-editor
* Domain Path: /languages
*/


class Auretto_Email_Editor {

	const PLUGIN_NAME = 'Auretto Email Template Editor';
	const TEXTDOMAIN = 'auretto-email-editor';
	const MAIN_CPT_NAME = 'auretto_email_editor';
	const MAIN_METABOX_NAME = 'auretto_email_editor_settings_metabox';
	const MAIN_SETTINGS_PAGE_NAME = 'default-email-template-editor-page';

	protected $plugin_dir_path, $template_dir_path, $plugin_dir_url;
	private $cpt, $actions, $filters, $meta_boxes, $save_post_datas;


	public function __construct() {


		/********************************/
		/*     SET GLOBAL VARIABLES     */
		/********************************/

		$this->plugin_dir_path = plugin_dir_path( __FILE__ );
		$this->plugin_dir_url = plugin_dir_url( __FILE__ );
		$this->template_dir_path = $this->plugin_dir_path .'templates/';

		/* --- */


		/***************************/
		/*     BASIC FUNCTIONS     */
		/***************************/

		$this->requireClasses( $this->plugin_dir_path .'includes/functions.php' );
		$this->functions = new AurettoEmailFunctions($this);

		$this->requireClasses( $this->plugin_dir_path .'includes/special_functions/import_previous_plugin_data_function.php' );
		$this->importPrevPluginFunctions = new AurettoEmailImportPrevPluginFunctions($this);

		$this->requireClasses( $this->plugin_dir_path .'includes/special_functions/send_email_function.php' );
		$this->sendMailFunction = new AurettoEmailSendMailFunction($this);

		/* --- */


		/*****************************/
		/*     CUSTOM POST TYPES     */
		/*****************************/

		$this->requireClasses( $this->plugin_dir_path .'includes/custom_post_types.php' );
		$this->cpt = new AurettoEmailCpt();
		add_action( 'init', array($this->cpt, 'registerPostType') );

		/* --- */


		/***********************/
		/*     ADD ACTIONS     */
		/***********************/

		$this->requireClasses( $this->plugin_dir_path .'includes/actions.php' );
		$this->actions = new AurettoEmailActions($this);

		// Textdomain
		add_action('init', array($this, 'load_textdomain'));

		// STYLES & SCRIPTS
		add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts_function'));

		// Define Editor Page
		add_action( 'admin_menu', array($this->actions, 'defaultEmailTemplateEditorPageMenuItem') );

		// Log Mailer Errors
		add_action( 'wp_mail_failed', array($this->actions, 'logMailerErrors'), 10, 1);

		// Admin Notices
		add_action( 'admin_notices', array($this->actions, 'incompleteSmtpData') );

		/* --- */


		/***********************/
		/*     ADD FILTERS     */
		/***********************/

		$this->requireClasses( $this->plugin_dir_path .'includes/filters.php' );
		$this->filters = new AurettoEmailFilters($this);

		// Template Redirects
		add_action( 'template_redirect', array($this->filters, 'templateRedirects') );

		/* --- */


		/**********************/
		/*     META BOXES     */
		/**********************/

		$this->requireClasses( $this->plugin_dir_path .'includes/meta_boxes.php' );
		$this->meta_boxes = new AurettoEmailMetaBoxes($this);

		$this->requireClasses( $this->plugin_dir_path .'includes/save_post_datas.php' );
		$this->save_post_datas = new AurettoEmailSavePostDatas($this);

		// Email Editor Settings Metabox
		add_action( 'add_meta_boxes', array($this->meta_boxes, 'addAurettoEmailEditorSettingsMetabox') );

		// Save Post Data
		add_action( 'save_post', array($this->save_post_datas, 'aurettoEmailEditorSavePostdatas') );

		/* --- */

	}


	/***************************/
	/*     OTHER FUNCTIONS     */
	/***************************/

	// Include file by path
	function requireClasses( $filePath = "" ) {
		if ( !empty($filePath) && file_exists($filePath) ) { require_once( $filePath ); }
	}

	function getPluginDirPath() 	{ return $this->plugin_dir_path; }
	function getPluginDirUrl() 		{ return $this->plugin_dir_url; }
	function getTemplateDirPath() { return $this->template_dir_path; }

	public function load_textdomain() {
		load_plugin_textdomain(Auretto_Email_Editor::TEXTDOMAIN, FALSE, dirname(plugin_basename(__FILE__)).'/languages/');
	}

	/* --- */


	/****************************/
	/*     STYLES & SCRIPTS     */
	/****************************/

	function admin_enqueue_scripts_function($hook) {
		$screen = get_current_screen();

		if ( $screen->post_type == Auretto_Email_Editor::MAIN_CPT_NAME ) {

			if ( !did_action('wp_enqueue_media') ) { wp_enqueue_media(); }
			wp_register_script('aee_admin_js', $this->plugin_dir_url .'js/admin.js');
			wp_enqueue_script('aee_admin_js');
			//wp_localize_script('aee_admin_js', 'aee', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

			wp_enqueue_style('aee_admin_css', $this->plugin_dir_url .'css/admin.css', array());
		}
	}

	/* --- */

}

new Auretto_Email_Editor();
